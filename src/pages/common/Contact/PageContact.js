import "./PageContact.scss";
import sytokki from '../../../assets/images/sytokki.PNG';
import happy from '../../../assets/images/samyangRamen_visual.jpg'
import CardUIProfile from "../../../components/Contents/CardUI/CardUIProfile/CardUIProfile";
import manimg1 from "../../../assets/images/man1.png";
import manimg2 from "../../../assets/images/man2.png";
//import CardUIProfile from "../../../components/Contents/CardUI/CardUIProfile/CardUIProfile";

const PageContact = () => {
    const jobDesc1 = "홍현욱 매니저는 삼양식품 경영지원본부 소속 IT/PI부문의 IT운영팀 MES 시스템 개발 및 운영 업무 담당 매니저입니다.\r\n\r\n\r\n\r\n\r\n\r\n\r\n회사번호: 02-940-3439\r\n내선번호: 3439";
    const jobDesc2 = "조강수 매니저는 삼양식품 경영지원본부 소속 IT/PI부문의 IT운영팀 MES 시스템 개발 및 운영 업무 담당 매니저입니다.\r\n\r\n\r\n\r\n\r\n\r\n\r\n회사번호: 02-940-3434\r\n내선번호: 3434";

    const imgStyle ={
        width: "200px", 
        border: "0px solid", 
        borderRadius: "24px", 
        marginTop: "10%"
    }

    return (
        <div id="pageContact">
            {/*<p>연락처⚡</p>*/}
            <CardUIProfile
                name="홍현욱 매니저"
                jobTitle="IT운영팀 / MES 개발 및 운영, 매니저"
                jobDesc={jobDesc1}
                img={<img src={manimg2} alt="desc" style={imgStyle} />}
            />
            <CardUIProfile
                name="조강수 매니저"
                jobTitle="IT운영팀 / MES 개발 및 운영, 매니저"
                jobDesc={jobDesc2}
                img={<img src={manimg1} alt="desc" style={imgStyle} />}
            />
        </div>
    );
};


export default PageContact;