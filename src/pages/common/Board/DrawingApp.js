import React, { useState, useRef, useReducer } from 'react';
import { Stage, Layer, Line } from 'react-konva';
import styled from 'styled-components';
import { v4 as uuidv4 } from 'uuid';

const drawingReducer = (state, action) => {
  switch (action.type) {
    case 'ADD_LINE':
      return { ...state, lines: [...state.lines, action.payload] };
    case 'UNDO':
      return { ...state, lines: state.lines.slice(0, -1) };
    case 'CLEAR':
      return { ...state, lines: [] };
    default:
      return state;
  }
};

const ClearButton = styled.button`
  background-color: #de2f86; /* 핑크색 */
  color: white;
  padding: 10px 20px;
  border: none;
  border-radius: 5px;
  margin-bottom: 10px;
  cursor: pointer;
`;

const SaveButton = styled.button`
  background-color: #4caf50; /* 초록색 */
  color: white;
  padding: 10px 20px;
  border: none;
  border-radius: 5px;
  margin-bottom: 10px;
  cursor: pointer;
`;

const DrawingApp = () => {
  const [state, dispatch] = useReducer(drawingReducer, { lines: [] });
  const [isDrawing, setDrawing] = useState(false)
  const stageRef = useRef();

  const handleMouseDown = (e) => {
    setDrawing(true);
    const pos = e.target.getStage().getPointerPosition();
    dispatch({ type: 'ADD_LINE', payload: { points: [pos.x, pos.y] } });
  };

  const handleMouseMove = (e) => {
    if (!isDrawing) return;

    const stage = e.target.getStage();
    const pointerPos = stage.getPointerPosition();

    const lastLine = state.lines[state.lines.length - 1];
    lastLine.points = lastLine.points.concat([pointerPos.x, pointerPos.y]);

    dispatch({ type: 'ADD_LINE', payload: lastLine });
  };

  const handleMouseUp = () => {
    setDrawing(false);
  };

  // const handleUndo = () => {
  //   dispatch({ type: 'UNDO' });
  // };

  const handleClear = () => {
    dispatch({ type: 'CLEAR' });
  };

  const today = new Date();
  const year = today.getFullYear();
  const month = String(today.getMonth() + 1).padStart(2, '0');
  const day = String(today.getDate()).padStart(2, '0');
  const minutes = String(today.getMinutes());
  const seconds = String(today.getSeconds());
  const milliseconds = String(today.getTime());

  // 1에서 10000 사이의 랜덤한 정수 생성
  const randomNumber = Math.floor(Math.random() * 10000) + 1;
  // 자릿수를 맞추기 위해 문자열로 변환
  const randomNumberString = randomNumber.toString();
  // 자릿수가 4자리가 되도록 앞에 0 추가
  const paddedRandomNumberString = randomNumberString.padStart(4, '0');
  // 고유넘버 생성 라이브러리
  const uniqueId = uuidv4();

   // 파일 이름에 오늘 날짜 추가
  const fileName = `my_drawing_${year}${month}${day}${minutes}${seconds}${milliseconds}${uniqueId}${paddedRandomNumberString}.png`;
  
  const handleSave = () => {
    // 로컬 스토리지에 현재 그림 저장
    // localStorage.setItem('savedDrawing', JSON.stringify(state.lines));
    // alert('Drawing saved!');

    const uri = stageRef.current.toDataURL();
    const link = document.createElement('a');
    link.href = uri;
    // link.download = 'my_drawing.png';
    link.download = fileName;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };


  return (
    <div>
      <h1>My Drawing</h1>
      <ClearButton onClick={handleClear}>Clear</ClearButton>
      <SaveButton onClick={handleSave}>Save</SaveButton>
     
      <Stage
        width={window.innerWidth}
        height={window.innerHeight - 50}
        onMouseDown={handleMouseDown}
        onMousemove={handleMouseMove}
        onMouseup={handleMouseUp}
        ref={stageRef}
      >
        <Layer>
          {state.lines.map((line, i) => (
            <Line key={i} points={line.points} stroke="black" strokeWidth={2} tension={0.5} />
          ))}
        </Layer>
      </Stage>
    </div>
  );
};

export default DrawingApp;
