import "./PageHome.scss";
import { Link as Router, Routes, Route } from 'react-router-dom';
import CardUIMain from "../../../components/Contents/CardUI/CardUIMain/CardUIMain";
// import PageEffort from '../../pages/common/Effort/PageEffort';
// import PageContact from '../../pages/common/Contact/PageContact';
// import DrawingApp from '../../pages/common/Board/DrawingApp';
import PageContact from '../Contact/PageContact';
import PageEffort from '../Effort/PageEffort';
import DrawingApp from '../Board/DrawingApp';

import imageFile1 from '../../../assets/images/contact.png';
import imageFile2 from '../../../assets/images/draing.png';
import imageFile3 from '../../../assets/images/customer.png';
import imageFile4 from '../../../assets/images/samyangRamen_visual.jpg';
import { Link } from "react-router-dom";

const PageHome = () => {
    /*const imageList = [
        <img src="https://pixabay.com/ko/photos/%ED%95%A0%EB%A1%9C%EC%9C%88-%EB%8F%85%EC%9D%BC-%EC%85%B0%ED%8D%BC%EB%93%9C-%EB%92%A4%EB%9C%B0-%EA%B0%9C-7553022/" alt="이미지 1"/>,
        <img src="https://pixabay.com/ko/photos/%ED%95%A0%EB%A1%9C%EC%9C%88-%EB%8F%85%EC%9D%BC-%EC%85%B0%ED%8D%BC%EB%93%9C-%EB%92%A4%EB%9C%B0-%EA%B0%9C-7553022/" alt="이미지 2"/>,
        <img src="https://pixabay.com/ko/photos/%ED%95%A0%EB%A1%9C%EC%9C%88-%EB%8F%85%EC%9D%BC-%EC%85%B0%ED%8D%BC%EB%93%9C-%EB%92%A4%EB%9C%B0-%EA%B0%9C-7553022/" alt="이미지 3"/>
    ];*/

    return (
        <Router>
            <div id="pageHome">
                <div>
                    <h1>삼양 휴카페 입니다.</h1>
                    <h>Gate of Communication
                    with Customer and samyang</h>
                </div>
                {/* <CardUIMain 
                    image={<img src={imageFile4}
                                alt="특징" 
                                />}                    
                     heading="삼양 휴 카페 입니다."
                    description="Test"
                />
                </div> */}
                <div className="pageHome-contents">
                    <Link to="/effort">
                        <CardUIMain
                            image={<img src={imageFile1}
                                alt="특징"
                            />}
                            heading="고객문의"
                            // description=""
                        />
                    </Link>
                    <Link to="/board/drawingApp">
                        <CardUIMain
                            image={<img src={imageFile2}
                                alt="특징"
                            />}
                            heading="그림 그리기"
                            // description="그림 그리기"
                        />
                    </Link>
                    <Link to="/contact">
                        <CardUIMain
                            image={
                                <img
                                    src={imageFile3}
                                    alt="특징"
                                />
                            }
                            heading="담당자 연락처"
                            // description="담당자 연락처"
                        />
                    </Link>
                </div>
            </div>
            <Routes>
                {/* 각 경로에 대한 컴포넌트를 정의하세요. */}
                <Route path="/effort" element={<PageEffort />} />
                <Route path="/contact" element={<PageContact />} />
                <Route path="/board/drawingApp" element={<DrawingApp />} />
            </Routes>
        </Router>
    );
};


export default PageHome;