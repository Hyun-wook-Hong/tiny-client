import React from 'react';
import './StdButton.scss';

const StdButton = ({ text, onClick, type, variant }) => {
    return(
            <button className="ok-button" 
                    onClick={onClick}
                    //type={type}
                    //variant={variant}
            >{text}</button>
    );

};

export default StdButton;