import React, { useState, useEffect } from 'react';
import BoardPost from "../BoardPost/BoardPost";
import StdButton from '../../Button/OKButton/StdButton';
import axios from 'axios';
import { Card, ListGroup } from "react-bootstrap";
import styled from 'styled-components';

const CardContainer = styled.div`
  width: 300px;
  margin: 10px;
  border: 1px solid #ddd;
  border-radius: 8px;
  overflow: hidden;
  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
`;

const CardHeader = styled.div`
  background-color: #555;
  color: white;
  padding: 10px;
  font-size: 18px;
  text-align: center;
`;

const ListGroupItem = styled.div`
  padding: 10px;
  border-bottom: 1px solid #ddd;

  &:last-child {
    border-bottom: none;
  }
`;

const BoardView = ({ postData }) => {
    const post = postData[0];

    // 게시글 수정 컴포넌트 활성화 state
    const [viewedFixedForm, setViewedFixedForm] = useState(false);
    const [targetData, setTargetData] = useState(null);

    // 게시글 수정:
    const onClickFixBtnHandler = () => {
      viewedFixedForm ? setViewedFixedForm(false) : setViewedFixedForm(true);
      setTargetData(post);
      console.log("viewedFixedForm: ", viewedFixedForm);
      console.log("Target Data: ", targetData);
    };

    // 게시글 삭제
    const onClickDeleteBtnHandler = async () => {
      setTargetData(post);

      if(window.confirm("정말 삭제하시겠습니까?")){
        // Y
        try{
          const response = await axios.delete(`http://localhost:18080/posts/${post.SERIAL_NO}`);
          console.log(response);
          window.location.reload();
        }catch(error){
          console.log("Unexpected exception caused. ", error);
        }
      };

    };

    return (
    <>
    { !viewedFixedForm ? 
    (
      <>
      <div className="post-contents-area">
        <CardContainer>
          <CardHeader>{post.REQ_STS}</CardHeader>
          <ListGroupItem>
            <strong>No. </strong> {post.SERIAL_NO}
          </ListGroupItem>
          <ListGroupItem>
            <strong>요청일자</strong> {post.REQ_DATE} {post.REQ_TIME}
          </ListGroupItem>
          <ListGroupItem>
            <strong>요청자</strong> {post.REQ_USER_NAME}
          </ListGroupItem>
          <ListGroupItem>
            <strong>요청내용</strong> {post.REQ_CONTENTS}
          </ListGroupItem>
          <ListGroupItem>
            <strong>요청자</strong> {post.REQ_USER_NAME}
          </ListGroupItem>
          <ListGroupItem>
            <strong>요청회사</strong> {post.REQ_COMP}
          </ListGroupItem>
          <ListGroupItem>
            <strong>처리내용</strong> {post.PRCS_CONTENTS}
          </ListGroupItem>
          <ListGroupItem>
            <strong>처리시간</strong> {post.PRCS_MIN} 분
          </ListGroupItem>
          <ListGroupItem>
            <strong>완료요청 일자</strong> {post.COMPLETE_REQ_DATE}
          </ListGroupItem>
          <ListGroupItem>
            <strong>처리완료 일자</strong> {post.PRCS_COMPLETE_DATE}
          </ListGroupItem>
          <CardHeader>
            <strong>실행회사</strong> {post.EXE_COMP}
          </CardHeader>
        </CardContainer>
        </div>
        <div className="bottom-btn-area"
             style={{ marginLeft: "70px" }}>
          <StdButton text="수정"
                     onClick={onClickFixBtnHandler}
          />
          <StdButton text="삭제"
                     onClick={onClickDeleteBtnHandler}
          />
        </div>          
      </>
  
    ) : (
        <>
        {<BoardPost data={targetData} postFlag={false}/>}
         <StdButton text="취소"
                    onClick={onClickFixBtnHandler}
          />      
        </>

) }

    </>
      );
};
export default BoardView;